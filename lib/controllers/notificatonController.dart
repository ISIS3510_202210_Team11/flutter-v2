import 'package:project/main.dart';
import 'package:flutter/services.dart';

class NotificationController 
{
String status = "Waiting...";
    final String success = "Created the channel successdully";
    final String error = "The channel could not be created";

    static const MethodChannel channel =
        MethodChannel('swapp.com/channel_test');

    Map<String, String> channelMap = {
      "id": "NOTIFICATIONS_FLUTTER",
      "name": "Notifications",
      "description": "Notifications",
    };

     void createNewChannel() async {
     
        await channel.invokeMethod('createNotificationChannel', channelMap);
    }
        
}