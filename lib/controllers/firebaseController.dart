//import 'dart:html';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:project/models/businessUser.dart';
import 'package:project/models/user.dart';
import 'package:project/models/businessUser.dart';
import 'package:project/models/product.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:project/models/transaction.dart' as trans;

class FirebaseController {
  static final FirebaseAuth auth = FirebaseAuth.instance;
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  FirebaseFirestore firestore= FirebaseFirestore.instance;
  static List<Product> products=[];

  void enablePersistence() {
    firestore.settings = Settings(persistenceEnabled: true);
    firestore.settings =
        Settings(cacheSizeBytes: Settings.CACHE_SIZE_UNLIMITED);
  }

  static Future<UserCredential> login(String email, String password) async {
    UserCredential credential =
        await auth.signInWithEmailAndPassword(email: email, password: password);
    return credential;
  }

  static Future<Map<String, dynamic>> getUser(String key) async {
    DocumentSnapshot documentsnapshot =
        await FirebaseFirestore.instance.collection('User').doc(key).get();
    print(documentsnapshot.data());
    return documentsnapshot.data()! as Map<String, dynamic>;
  }


  static Future<Map<String, dynamic>> getItem(String key) async {
    DocumentSnapshot documentsnapshot =
        await FirebaseFirestore.instance.collection('Product').doc(key).get();
    print(documentsnapshot.data());
    return documentsnapshot.data()! as Map<String, dynamic>;
  }


  Future<List<Product>> getProducts() async 
       {  
       QuerySnapshot<Map<String, dynamic>> snapshot = await firestore.collection('Product').get();
       List<QueryDocumentSnapshot<Map<String, dynamic>>> docs = snapshot.docs;
       late Product product;
       docs.forEach((element) {
         product = Product(size: element["size"], name: element["name"], isSwappable: element["isSwappable"], state: element["state"], img: element["img"], price: element["suggestedPrice"], tags: element["tags"]);
         products.add(product);
       });

        return products;
       }

  static Future<void> addItem(Product p) async {
    CollectionReference items =
        await FirebaseFirestore.instance.collection('Product');

    return items
        .add({
          'isSwappable': p.isSwappable, // John Doe
          'name': p.name, // Stokes and Sons
          'size': p.size,
          'state': p.state,
          'suggestedPrice': p.price,
          'tags': p.tags,
          'img': p.img,
        })
        .then((value) => print("item added"))
        .catchError((error) => print("Item NOT added: $error"));
  }
  static Future<void> addUserPerson(appUser u, String uid) {
    CollectionReference users =
    FirebaseFirestore.instance.collection('User');

    return users
    .doc(uid)
        .set({
      'name': u.name, // John Doe
      'address': u.address, // Stokes and Sons
      'birthDate': u.birthDate,
      'city': u.city,
      'gender': u.gender,
      'type': u.type,
      'userName': u.userName,
      'profilePicture':u.imgPath,
      'averageRating':u.averageRaiting,
      'posts':u.posts,
      'reviews':u.reviews,
      'textileWasteSaved':u.savedTextWaste,
      'transactions':u.transactions,
      'treesPlanted':u.treesPlanted,
      'password':'',


    })
        .then((value) => print("user added"))
        .catchError((error) => print("user NOT added: $error"));
  }
  static Future<void> addUserBusiness(businessUser u, String uid) {
    CollectionReference users =
    FirebaseFirestore.instance.collection('User');

    return users
        .doc(uid)
        .set({
      'name': u.name, // John Doe
      'address': u.address,
      'city': u.city,
      'type': u.type,
      'userName': u.userName,
      'profilePicture':u.imgPath,
      'averageRating':u.averageRaiting,
      'posts':u.posts,
      'reviews':u.reviews,
      'transactions':u.transactions,
      'nit':u.nit,
      'website':u.website,
      'description':u.description,
      'category':u.category,
      'password':'',



    })
        .then((value) => print("user added"))
        .catchError((error) => print("user NOT added: $error"));
  }


  static Future<void> addTransaction(trans.Transaction t) async {
    CollectionReference transactions =
        await FirebaseFirestore.instance.collection('Transaction');

    return transactions
        .add({
          'payment_type': t.paymentType,
          'promotion_code': t.promotionCode,
          'total': t.total,
        })
        .then((value) => print("transaction added"))
        .catchError((error) => print("transaction NOT added: $error"));
  }
}
