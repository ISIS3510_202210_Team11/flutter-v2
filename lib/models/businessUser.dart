import 'package:flutter/cupertino.dart';
import 'package:project/models/product.dart';
import 'package:project/models/review.dart';

class businessUser {
  final String address;
  final String category;
  final String description;
  final String nit;
  final String website;
  final double averageRaiting;
  final String city;
  final String imgPath;
  final String name;
  final String userUID;
  final List<String> posts;
  final List <String> reviews;
  final String type;
  final String userName;
  final List<String> transactions;


  const businessUser ({
    required this.address,
    required this.averageRaiting,
    required this.city,
    required this.imgPath,
    required this.name,
    required this.userUID,
    required this.nit,
    required this.description,
    required this.website,
    required this.category,
    required this.type,
    required this.userName,
    required this.transactions,
    required this.posts,
    required this.reviews,


  });

  static businessUser fromJson(Map <String, dynamic>map, String uid)
  {
    return businessUser(

      imgPath: map["profilePicture"],
      name: map["name"],
      userUID: uid,
      nit: map ["nit"],
      category: map["category"],
      description: map["description"],
      website: map["website"],
      address: map["address"],
      averageRaiting: map["averageRaiting"],
      city: map ["city"],
      posts:map["posts"],
      reviews: map ["reviews"],
      transactions: map ["transactions"],
      userName: map ["userName"],
      type: map["type"],



    );
  }

  Map<String, Object?> toJson() {
    return {
      'profilePicture': imgPath,
      'name': name,
      'category': category,
      'description': description,
      'website': website,
      'nit': nit,
      'city': city,
      'address': address,
      'averageRaiting': averageRaiting,
      'posts': posts,
      'reviews': reviews,
      'transactions': transactions,
      'userName': userName,
      'type': type,

    };
  }
}