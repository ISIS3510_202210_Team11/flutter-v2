import 'package:flutter/cupertino.dart';
import 'package:project/models/product.dart';
import 'package:project/models/review.dart';

class appUser {
final String address;
final double averageRaiting;
final String city;
final String imgPath;
final String name;
final String userUID;
final String gender;
final String birthDate;
final String size;
final int treesPlanted;
final int savedTextWaste;
final List<dynamic> posts;
final List <dynamic> reviews;
final String type;
final String userName;
final List<dynamic> transactions;


const appUser ({
required this.address,
required this.averageRaiting,
required this.city,
required this.imgPath,
required this.name,
required this.userUID,
required this.gender,
required this.birthDate,
required this.size,
required this.treesPlanted,
required this.savedTextWaste,
required this.type,
required this.userName,
required this.transactions, 
required this.posts,
required this.reviews,


});

static appUser fromJson(Map <String, dynamic>map, String uid)
{
  return appUser(

    imgPath: map["profilePicture"],
    name: map["name"],
    userUID: uid,
    gender: map ["gender"],
    birthDate: map["birthDate"],
    size: map["size"],
    treesPlanted: map["treesPlanted"],
    savedTextWaste: map["textileWasteSaved"],
    address: map["address"],
    averageRaiting: map["averageRaiting"],
    city: map ["city"],
    posts:map["posts"],
    reviews: map ["reviews"],
    transactions: map ["transactions"],
    userName: map ["userName"],
    type: map["type"],



  );
}

Map<String, Object?> toJson() {
    return {
      'profilePicture': imgPath,
      'name': name,
      'gender': gender,
      'birthDate': birthDate,
      'size': size,
      'treesPlanted': treesPlanted,
      'textileWasteSaved': savedTextWaste,
      'city': city,
      'address': address,
      'averageRaiting': averageRaiting,
      'posts': posts,
      'reviews': reviews,
      'transactions': transactions,
      'userName': userName,
      'type': type,



    };
  }
}