class Transaction {
  String paymentType;
  String promotionCode;
  double total;

  Transaction(
      {required this.paymentType,
      required this.promotionCode,
      required this.total});
}
