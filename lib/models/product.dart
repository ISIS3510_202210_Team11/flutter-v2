import 'package:flutter/cupertino.dart';

class Product {
  String size;
  String name;
  bool isSwappable;
  String state;
  String img;
  double price;
  List<dynamic> tags;
  //final String itemId;

   Product(
      {required this.size,
      required this.name,
      required this.isSwappable,
      required this.state,
      required this.img,
      required this.price,
      //required this.itemId,
      required this.tags});


static Product fromJson(Map <String, dynamic>map, String iid)
{
  return Product(

    size: map["size"],
    name: map["name"],
    //itemId: iid,
    state: map ["state"],
    img: map["img"],
    price: map["suggestedPrice"],
    tags: map["tags"],
    isSwappable:map["isSwappable"],



  );
}

Map<String, Object?> toJson() {
    return {

      'size': size,
      'name': name,
      'state': state,
      'img': img,
      'suggestedPrice': price,
      'tags': tags,
      'isSwappable': isSwappable,



    };
  }
}