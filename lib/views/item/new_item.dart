import 'dart:convert';
import 'dart:typed_data';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:project/views/item/imageWidget.dart';
import 'dart:io';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:project/controllers/firebaseController.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:project/models/product.dart';
import 'package:project/views/item/new_item.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:project/views/item/progressBar.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

FirebasePerformance performance = FirebasePerformance.instance;
/*
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}
 */

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: CreateItem());
  }
}

class CreateItem extends StatefulWidget {
  /*
  const MyHome({
    Key? key,
    required this.imagen,
  }) : super(key: key);
  */

  @override
  _CreateItemState createState() => _CreateItemState();
}

class _CreateItemState extends State<CreateItem> {
  Trace trace = performance.newTrace('newItem');

  @override
  void initState() {
    // TODO: implement initState
    trace.start();
    super.initState();
  }

  //states con los campos del formulario
  String pSize = '';
  String pName = '';
  bool pSwappable = false;
  String pState = '';
  String pImg = '';
  double pPrice = 0;
  List<String> pTags = [];

  //state con el estado de conectividad
  ConnectivityResult result = ConnectivityResult.none;

  Product item = Product(
      size: "Large",
      name: "White dress",
      isSwappable: true,
      state: "new",
      img: "...",
      price: 3,
      tags: ['Levis', 'classic']);
  File fotoNew = File('assets/images/dress.jpeg');
  bool selectPhoto = false;

  UploadTask? uploadImage;

  //Function which opens the image widget. Espera a que le devuelvan la imagen
  void _handleURLButtonPress(BuildContext context, var type) async {
    final File imagen = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => ImageFromGalleryEx(type)));

    //Asigna la imagen que el usuario escogió

    setState(() {
      fotoNew = imagen;
      if (fotoNew != null) {
        selectPhoto = true;
      }
    });
  }

  //Funcion para subir archivo (foto) a firestore. Multithreading.
  Future<String> uploadFile() async {
    //referencia de storage
    final storageRef = FirebaseStorage.instance.ref();

    //referencia al archivo. Path en storage => nombre del item
    final imagen_upload_ref = storageRef.child(item.name);

    //Subir archivo
    uploadImage = imagen_upload_ref.putFile(fotoNew);

    //snapshoot que dice si la imagen ya se subio
    final snapshot = await uploadImage!.whenComplete(() {});

    //descargar url archivo
    final urlDownload = await snapshot.ref.getDownloadURL();
    return urlDownload;
  }

  @override
  Widget build(BuildContext context) {
    trace.stop();
    return Scaffold(
      backgroundColor: Color(0xffE5E5E5),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                ],
              ),
              //Title that says new item
              Text(
                'New Item',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
              ),

              //Container to show image
              Container(
                height: 300,
                width: 300,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  //Si no hay unna foto seleccionada, muestre el vestido.
                  image: selectPhoto
                      ? DecorationImage(image: Image.file(fotoNew).image)
                      : DecorationImage(
                          image: Image.asset("assets/images/dress.jpeg").image),
                  //Image.asset(widget.imagen.path).image,
                ),
              ),

              //Fila de Botones: Galeria o foto
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 218, 146, 82),
                        minimumSize: const Size(100, 35),
                        maximumSize: const Size(100, 35)),
                    child: Text('Gallery'),
                    onPressed: () {
                      _handleURLButtonPress(context, ImageSourceType.gallery);
                    }),
                SizedBox(width: 50),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 218, 146, 82),
                        minimumSize: const Size(100, 35),
                        maximumSize: const Size(100, 35)),
                    child: Text('Camera'),
                    onPressed: () {
                      _handleURLButtonPress(context, ImageSourceType.camera);
                    })
              ]),

              //Caja para crear espacio
              SizedBox(
                height: 20,
                width: 200,
                child: Divider(color: Colors.white),
              ),

              //Card for the Form
              Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 65),
                child: Form(
                  child: Column(
                    children: [
                      //name form entry
                      Padding(
                        padding: EdgeInsets.fromLTRB(20.0, 0, 20, 5),
                        child: TextFormField(
                          //Funcion que dice que pasa cuando el campo cambie. val: Value del formfield
                          onChanged: (val) {
                            setState(() => pName = val);
                            item.name = pName;
                          },
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Product Name',
                          ),
                        ),
                      ),

                      //Size form entry
                      Padding(
                        padding: EdgeInsets.fromLTRB(20.0, 0, 20, 5),
                        child: TextFormField(
                          onChanged: (val) {
                            setState(() => pSize = val);
                            item.size = pSize;
                          },
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Size',
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20.0, 0, 20, 5),
                        child: TextFormField(
                          onChanged: (val) {
                            setState(() => pPrice = double.parse('val'));
                            item.price = pPrice;
                          },
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Price',
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20.0, 0, 20, 5),
                        child: TextFormField(
                          onChanged: (val) {
                            setState(() => pState = val);
                            item.state = pState;
                          },
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'New or used',
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20.0, 0, 20, 5),
                        child: TextFormField(
                          onChanged: (val) {
                            setState(() => pTags = val.split(','));
                            item.tags = pTags;
                          },
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Tags',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              // Buttons Swappable and done
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                //Boton swappable
                SizedBox(
                  width: 125.0,
                  height: 30.0,
                  child: LiteRollingSwitch(
                    onChanged: (bool position) {
                      //Esto lo hace para que setState no interrumpa el runtime
                      WidgetsBinding.instance
                          ?.addPostFrameCallback((timeStamp) {
                        setState(() => pSwappable = position);
                      });
                      item.isSwappable = pSwappable;
                    },
                    value: false,
                    textOn: "Swappable",
                    textOff: "Swappable",
                    colorOn: Colors.greenAccent,
                    colorOff: Colors.redAccent,
                    iconOn: Icons.done,
                    iconOff: Icons.do_disturb,
                    textSize: 15,
                  ),
                ),
                SizedBox(width: 50),
                //Boton done
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 218, 146, 82),
                        minimumSize: const Size(100, 35),
                        maximumSize: const Size(100, 35)),
                    child: Text('Done'),
                    //Enviar el formulario. Debe ser async pq interactua con firebase
                    onPressed: () async {
                      //Chekea conectividad
                      result = await Connectivity().checkConnectivity();
                      if (result == ConnectivityResult.none) {
                        final snackBar = SnackBar(
                          content: const Text(
                              'No active connection. Item not uploaded failed :('),
                          action: SnackBarAction(
                            label: 'Ok',
                            onPressed: () {},
                          ),
                        );

                        //Mostrar snackbar
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      } else {
                        item.img = await uploadFile();
                        FirebaseController.addItem(item);
                        final snackBar = SnackBar(
                          content: const Text(
                              'Your item was uploaded succesfully :)'),
                          action: SnackBarAction(
                            label: 'Ok',
                            onPressed: () {},
                          ),
                        );
                        //Mostrar snack bar
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      }

                      print(item.name);
                      print(item.size);
                      print(item.isSwappable);
                      print(item.state);
                      print(item.price);
                      print(item.tags);
                      print(item.img);
                    }),
                //buildProgress(uploadImage),
              ]),
            ],
          ),
        ),
      ),
    );
  }
}
