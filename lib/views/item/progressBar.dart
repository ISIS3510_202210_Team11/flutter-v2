import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget buildProgress(image) => StreamBuilder<TaskSnapshot>(
    stream: image?.snapshotEvents,
    builder: (context, snapshot) {
      if (snapshot.hasData) {
        final data = snapshot.data!;
        double progreso = data.bytesTransferred / data.totalBytes;

        return SizedBox(
          height: 50,
          child: Stack(
            fit: StackFit.expand,
            children: [
              LinearProgressIndicator(
                value: progreso,
                backgroundColor: Colors.grey,
                color: Colors.green,
              ),
              Center(
                child: Text(
                  '${(100 * progreso).roundToDouble()}%',
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        );
      } else {
        return const SizedBox(height: 10);
      }
    });
