import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:project/models/product.dart';
import 'package:project/views/home/homepage.dart';
import 'package:project/views/cart/cart.dart';
import 'package:project/views/search/search.dart';
import 'package:project/models/globals.dart' as globals;


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
}

class MainMenu extends StatefulWidget {
  const MainMenu({
    Key? key,
    required this.uid,
    required this.location,
    required this.itemId,

  })  : super(key: key);
  final String uid;
  final String location;
  final List<Product> itemId;
  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  int selectedIndex = 0;
  late String id = widget.uid;
  late String location = widget.location;
  late List<Product> itemId= widget.itemId;

  //list of widgets to call ontap

  late final widgetOptions = [
    new HomePage(uid: id, location: location, itemList: itemId),
    new search(),
    new Cart(globals.cart,id,location),
  ];

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  final widgetTitle = ["Home", "Search", "Cart"];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home:Scaffold(
          body: Center(
            child: widgetOptions.elementAt(selectedIndex),
          ),
          bottomNavigationBar: BottomNavigationBar(
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.home,
                  ),
                  label: ""),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.search_outlined,
                  ),
                  label: ""),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.shopping_bag_outlined,
                  ),
                  label: ""),
            ],
            currentIndex: selectedIndex,
            onTap: onItemTapped,
            selectedIconTheme: IconThemeData(color: Colors.black, opacity: 1.0, size: 35.0),
            unselectedIconTheme: IconThemeData(color: Colors.grey, opacity: 1.0, size: 30.0),
          ),
        )
    );
  }

}
