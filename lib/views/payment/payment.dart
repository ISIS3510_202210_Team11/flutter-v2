import 'dart:ffi';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:project/views/payment/cardsWidget.dart';
import 'package:project/views/cart/cart.dart';
import 'package:intl/intl.dart';
import 'package:project/models/transaction.dart';
import 'package:project/controllers/firebaseController.dart';
import 'package:project/views/payment/notification.dart';
import 'package:project/models/product.dart';
import 'package:project/views/menu/menu.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  //runApp(Payment(uid: 'KXitKhuh6uhMbB3XfVgB0FZpy8w1', location: "bogota"));
}

//Clase Payment Widget Stateless
class Payment extends StatelessWidget {
  const Payment({
    Key? key,
    required this.uid,
    required this.location,
    required this.products,
    required this.total,
  }) : super(key: key);

  final String uid;
  final String location;
  final List<Product> products;
  final double total;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white24,
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 1),
            child: PaymentPage(
                uid: uid,
                location: location,
                products: products,
                ptotal: total),
          ),
        ),
      ),
    );
  }
}

class PaymentPage extends StatefulWidget {
  const PaymentPage({
    Key? key,
    required this.uid,
    required this.location,
    required this.products,
    required this.ptotal,
  }) : super(key: key);

  final String uid;
  final String location;
  final List<Product> products;
  final double ptotal;
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  @override
  void initState() {
    super.initState();
    NotificationApi.init();
    //Busca primero en el cache
    getCardSharedPref();
  }

  //Ubicacion del usuario
  String location = "Bogota";
  //double subtotalVar = PaymentPage.ptotal;
  //String subTotalStr = subtotalVar.toString();
  List<Product> productsQuemado = [];

  //States con loos campos a persistir
  String chosenCard = '';
  String promotionCode = '';
  double totalPrice = 88000; //Este se lo tienen que pasar de cart

  //state para saber conectividad del dispositivo
  ConnectivityResult result = ConnectivityResult.none;

  //Traer el medio de pago del shared preferences
  Future<void> getCardSharedPref() async {
    final prefs = await SharedPreferences.getInstance();
    final preferedCard = prefs.getString('preferedCard');
    if (preferedCard == null) {
      chosenCard = '';
    } else {
      setState(() {
        chosenCard = preferedCard;
      });
      print(chosenCard);
    }
  }

  //Guardar medio de pago reciente en shared preferences
  Future<void> addMethodSharedPref(value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('preferedCard', value);
  }

  //objeto transaction
  Transaction transaction = Transaction(
      paymentType: 'payment_type',
      promotionCode: 'promotion_code',
      total: 88000);

  static final DateTime now = DateTime.now();
  static final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final String formattedTime = formatter.format(now);

  //nueva tarjeta
  String dropValue = 'American Express';
  Future<void> addCard(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: DropdownButton(
              value: dropValue,
              items: ['American Express', 'Paypal', 'Citibank', 'PayU']
                  .map((String i) {
                return DropdownMenuItem(value: i, child: Text(i));
              }).toList(),
              onChanged: (String? newVal) {
                setState(() {
                  dropValue = newVal!;
                  chosenCard = dropValue;
                  transaction.paymentType = chosenCard;
                  //anade al shared pref
                  addMethodSharedPref(chosenCard);
                });
              },
            ),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('ok'))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffE5E5E5),
      body: SafeArea(
          child: SingleChildScrollView(
              child: Column(
        children: <Widget>[
          Row(
            children: [
              IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => MainMenu(
                            uid: widget.uid,
                            location: location,
                            itemId: widget.products),
                      ),
                    );
                  }),
              SizedBox(width: 120),
              Text("Checkout",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Color.fromARGB(255, 49, 68, 93))),
            ],
          ),

          //Espacio entre appbar y contenido
          SizedBox(height: 10),
          //Ubicacion y tiempo
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //Icono de lugar
              const Icon(Icons.place,
                  color: Color.fromARGB(255, 49, 68, 93), size: 30),
              //Ubicacion del usuario
              Text(location,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Color.fromARGB(255, 49, 68, 93))),
              const SizedBox(width: 100),
              const Icon(Icons.punch_clock,
                  color: Color.fromARGB(255, 49, 68, 93), size: 30),
              Text(formattedTime,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Color.fromARGB(255, 49, 68, 93))),
            ],
          ),
          //Payment method
          Container(
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text("Payment: $chosenCard",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Color.fromARGB(255, 49, 68, 93))),
              ),
            ),
          ),
          //Slider de los medios de pago
          Container(
            height: 100,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Container(
                    width: 80,
                    child: ElevatedButton.icon(
                      icon: Icon(
                        Icons.add,
                        size: 40.0,
                      ),
                      onPressed: () {
                        addCard(context);
                      },
                      label: Text(''),
                    ),
                  ),
                ),
                Container(
                  width: 180,
                  child: InkWell(
                    splashColor: Color(0xffE5E5E5),
                    onTap: () {
                      setState(() {
                        chosenCard = 'Mastercard';
                      });
                      transaction.paymentType = chosenCard;
                      //anade al shared pref
                      addMethodSharedPref(chosenCard);
                    },
                    child: Ink.image(
                        image:
                            Image.asset('assets/images/mastercard.png').image),
                  ), //Otra forma de poner image widgets
                ),
                Container(
                    width: 180,
                    child: InkWell(
                      splashColor: Color(0xffE5E5E5),
                      onTap: () {
                        setState(() {
                          chosenCard = 'Visa';
                        });
                        transaction.paymentType = chosenCard;
                        //anade al shared pref
                        addMethodSharedPref(chosenCard);
                      },
                      child: Ink.image(
                          image: Image.asset('assets/images/visa.png').image),
                    ) //Otra forma de poner image widgets
                    ),
              ],
            ),
          ),

          //
          //
          SizedBox(height: 20),
          //Texto Promotion code
          Container(
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text("Promotion Code",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Color.fromARGB(255, 49, 68, 93))),
              ),
            ),
          ),

          //Promotion code
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 15, top: 0),
            child: Row(
              children: [
                //Text field para poner el promotion code
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(5.0, 0, 5, 5),
                    child: TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Code',
                        ),
                        onChanged: (val) {
                          setState(() => promotionCode = val);
                          transaction.promotionCode = promotionCode;
                        }),
                  ),
                ),
                //Boton para add
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Colors.white30,
                        side: BorderSide(width: 0.5, color: Colors.black54),
                        minimumSize: const Size(100, 50),
                        maximumSize: const Size(100, 50)),
                    child: Text('Add',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Color.fromARGB(255, 49, 68, 93))),
                    onPressed: () {})
              ],
            ),
          ),

          //Filas de precios subtotal
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Column(
              children: [
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text("Subtotal",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            color: Color.fromARGB(255, 49, 68, 93))),
                    Text('83000'),
                  ],
                ),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text("Delivery",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            color: Color.fromARGB(255, 49, 68, 93))),
                    Text("5,000")
                  ],
                ),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text("Total",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                            color: Color.fromARGB(255, 218, 146, 82))),
                    Text("88,500",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                            color: Color.fromARGB(255, 218, 146, 82)))
                  ],
                )
              ],
            ),
          ),

          //Boton de pagar
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Color(0xff31445D),
                  side: BorderSide(width: 0.5, color: Colors.black54),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  minimumSize: const Size(200, 64),
                  maximumSize: const Size(200, 64)),
              child: Text('Pay',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white)),
              onPressed: () async {
                result = await Connectivity().checkConnectivity();
                if (result == ConnectivityResult.none) {
                  final snackBar = SnackBar(
                    content:
                        const Text('No active connection. Payment failed :('),
                    action: SnackBarAction(
                      label: 'Ok',
                      onPressed: () {},
                    ),
                  );

                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                } else {
                  FirebaseController.addTransaction(transaction);
                  NotificationApi.showNotification(
                    title: "Your purchase has been registered",
                    body: "Enjoy your product!",
                    payload: 'payload',
                  );
                  final snackBar = SnackBar(
                    content:
                        const Text('Payment succesful. Enjoy your product :)'),
                    action: SnackBarAction(
                      label: 'Ok',
                      onPressed: () {},
                    ),
                  );
                  //Mostrar snack bar
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
              })
        ],
      ))),
    );
  }
}
