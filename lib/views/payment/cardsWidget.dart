import 'package:flutter/material.dart';

//Clase Payment Widget Stateless
class CardsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color(0xffE5E5E5),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: TarjetaPage(),
          ),
        ),
      ),
    );
  }
}

class TarjetaPage extends StatefulWidget {
  @override
  _TarjetaPageState createState() => _TarjetaPageState();
}

class _TarjetaPageState extends State<TarjetaPage> {
  //tarjeta escogida
  String tarjeta = "Mastercard";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffE5E5E5),
      body: SafeArea(
          child: SingleChildScrollView(
              child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Payment"),
              ListView(
                scrollDirection: Axis.horizontal,
                //Slider de los medios de pago
                children: <Widget>[
                  Container(
                    width: 200,
                    color: Colors.purple[600],
                    child: Image.asset(
                        'images/mastercard.png'), //Otra forma de poner image widgets
                  ),
                ],
              ),
            ],
          ),
          //Payment method
        ],
      ))),
    );
  }
}
