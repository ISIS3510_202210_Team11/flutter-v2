import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:project/views/Register/business.dart';
import 'package:project/views/Register/person.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

import '../splashview.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  //runApp( signup());
}

final _auth = FirebaseAuth.instance;
FirebaseAnalytics analytics = FirebaseAnalytics.instance;
FirebasePerformance performance = FirebasePerformance.instance;
String email = '';
String password = '';
String name = '';
String city = '';
String address = '';
String type = '';

class signup extends StatefulWidget {
  signup({Key? key}) : super(key: key);

  @override
  State<signup> createState() => MyApp();
}

class MyApp extends State<signup> {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(home: _state());
  }
}

class _state extends StatefulWidget {
  const _state({Key? key}) : super(key: key);

  @override
  State<_state> createState() => _signupState();
}

class _signupState extends State<_state> {
  final _formKey = GlobalKey<FormState>();
  Trace trace = performance.newTrace('Register');

  @override
  void initState() {
    // TODO: implement initState
    trace.start();
  }
  Widget build(BuildContext context) {
    trace.stop();
    return MediaQuery(
      data: new MediaQueryData(),
      child: MaterialApp(
        home: Scaffold(
            body: Directionality(
                textDirection: TextDirection.ltr,
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: ListView(
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: IconButton(
                              icon: const Icon(Icons.keyboard_return),
                              onPressed: () async {
                                await Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (contex) => SplashViewPage(),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                        Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                                Container(
                                    alignment: Alignment.topLeft,
                                    padding: const EdgeInsets.all(10),
                                    child: const Text(
                                      'Register',
                                      style: TextStyle(
                                        fontSize: 36,
                                        fontFamily: 'RobotoMono',
                                        fontWeight: FontWeight.bold,
                                        color: Color.fromARGB(255, 49, 68, 93),
                                      ),
                                    )),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  child: TextFormField(
                                    validator: (name) {
                                      if (name == null ||
                                          name.isEmpty) {
                                        return 'Name is empty';
                                      }
                                      return null;
                                    },
                                    keyboardType: TextInputType.name,
                                    onChanged: (value) {
                                      name = value;
                                    },
                                    style: const TextStyle(
                                        fontSize: 15, fontFamily: 'RobotoMono'),
                                    decoration: const InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color.fromARGB(
                                                255, 218, 146, 82),
                                            width: 1.8),
                                      ),
                                      labelText: 'Name',
                                      labelStyle: TextStyle(
                                        color:
                                            Color.fromARGB(255, 218, 146, 82),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  child: TextFormField(
                                    validator: (city) {
                                      if (city == null ||
                                          city.isEmpty) {
                                        return 'City is empty';
                                      }
                                      return null;
                                    },
                                    keyboardType: TextInputType.text,
                                    onChanged: (value) {
                                      city = value;
                                    },
                                    style: const TextStyle(
                                        fontSize: 15, fontFamily: 'RobotoMono'),
                                    decoration: const InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color.fromARGB(
                                                255, 218, 146, 82),
                                            width: 1.8),
                                      ),
                                      labelText: 'City',
                                      labelStyle: TextStyle(
                                        color:
                                            Color.fromARGB(255, 218, 146, 82),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  child: TextFormField(
                                    validator: (address) {
                                      if (address == null ||
                                          address.isEmpty) {
                                        return 'Address is empty';
                                      }
                                      return null;
                                    },
                                    keyboardType: TextInputType.text,
                                    onChanged: (value) {
                                      address = value;
                                    },
                                    style: const TextStyle(
                                        fontSize: 15, fontFamily: 'RobotoMono'),
                                    decoration: const InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color.fromARGB(
                                                255, 218, 146, 82),
                                            width: 1.8),
                                      ),
                                      labelText: 'Address',
                                      labelStyle: TextStyle(
                                        color:
                                            Color.fromARGB(255, 218, 146, 82),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  child: TextFormField(
                                    validator: (x) {
                                      if (email.isEmpty || !email.contains('@')) {
                                        return 'Enter a valid e-mail';
                                      }
                                      return null;
                                    },
                                    keyboardType: TextInputType.emailAddress,
                                    onChanged: (value) {
                                      email = value;
                                    },
                                    style: const TextStyle(
                                        fontSize: 15, fontFamily: 'RobotoMono'),
                                    decoration: const InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color.fromARGB(
                                                255, 218, 146, 82),
                                            width: 1.8),
                                      ),
                                      labelText: 'E-mail',
                                      labelStyle: TextStyle(
                                        color:
                                            Color.fromARGB(255, 218, 146, 82),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                  child: TextFormField(
                                    validator: (password) {
                                      if (password == null ||
                                          password.isEmpty) {
                                        return 'Password is empty';
                                      } else if (password.length < 6) {
                                        return 'Password must have at least 6 characters';
                                      }
                                      return null;
                                    },
                                    obscureText: true,
                                    onChanged: (value) {
                                      password = value;
                                    },
                                    decoration: const InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color.fromARGB(
                                                255, 218, 146, 82),
                                            width: 1.8),
                                      ),
                                      labelText: 'Password',
                                      labelStyle: TextStyle(
                                          color:
                                              Color.fromARGB(255, 218, 146, 82),
                                          fontFamily: 'RobotoMono'),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Column(children: [
                                    Row(children: [
                                      Expanded(
                                          child: ChoiceChip(
                                              label: Text('Normal Account',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14)),
                                              labelPadding:
                                                  EdgeInsets.symmetric(
                                                      horizontal: 20),
                                              selected: type == 'Person',
                                              onSelected: (bool selected) {
                                                setState(() {
                                                  type = (selected
                                                      ? 'Person'
                                                      : null)!;
                                                });
                                              },
                                              selectedColor: Color.fromARGB(
                                                  255, 218, 146, 82),
                                              shape: ContinuousRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          50.0)))),
                                      Expanded(
                                          child: ChoiceChip(
                                              label: Text('Business Acount',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14)),
                                              labelPadding:
                                                  EdgeInsets.symmetric(
                                                      horizontal: 20),
                                              selected: type == 'Business',
                                              onSelected: (bool selected) {
                                                setState(() {
                                                  type = (selected
                                                      ? 'Business'
                                                      : null)!;
                                                });
                                              },
                                              selectedColor: Color.fromARGB(
                                                  255, 218, 146, 82),
                                              shape: ContinuousRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          30.0))))
                                    ]),
                                  ]),
                                ),
                              ],
                            )),
                        Container(
                            height: 50,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: ElevatedButton(
                                style: TextButton.styleFrom(
                                  backgroundColor: Color.fromARGB(
                                      255, 49, 68, 93), // Background Color
                                ),
                                child: const Text(
                                  'NEXT',
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontFamily: 'RobotoMono',
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () async {
                                  try {
                                    //UserCredential credentials= await _auth.createUserWithEmailAndPassword(
                                    //    email: email,
                                    //    password: password
                                    // );
                                    //String id=credentials.user!.uid;
                                    if (_formKey.currentState!.validate()) {
                                      if (type == 'Business') {
                                        await Navigator.push(
                                          context,
                                          new MaterialPageRoute(
                                            builder: (contex) => new business(
                                                email: email,
                                                name: name,
                                                type: type,
                                                city: city,
                                                adress: address,
                                                password: password),
                                          ),
                                        );
                                      } else if (type == 'Person') {
                                        await Navigator.push(
                                          context,
                                          new MaterialPageRoute(
                                            builder: (contex) => new person(
                                              email: email,
                                              name: name,
                                              type: type,
                                              city: city,
                                              adress: address,
                                              password: password,
                                            ),
                                          ),
                                        );
                                      }
                                    }
                                  } on FirebaseAuthException catch (e) {
                                    showDialog(
                                        context: context,
                                        builder: (ctx) => AlertDialog(
                                              title: Text("Ops! Failed"),
                                            ));
                                  }
                                  ;
                                })),
                      ],
                    )))),
      ),
    );
  }
}
