import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:geolocator/geolocator.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:project/models/businessUser.dart';
import 'package:project/views/Register/signup.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project/models/product.dart';

import 'package:project/models/globals.dart' as globals;
import '../../controllers/firebaseController.dart';
import '../menu/menu.dart';

enum ImageSourceType { gallery, camera }

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
 // runApp( business());

}

final _auth = FirebaseAuth.instance;
FirebaseAnalytics analytics = FirebaseAnalytics.instance;

String nit = '';
String url = '';
String category = '';
String description = '';
String img = '';


late String currentAddress;
late List<Product> items = [];
Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
FirebasePerformance performance = FirebasePerformance.instance;


getCurrentLocation()async {


  Position position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);

  List<Placemark> p = await geolocator.placemarkFromCoordinates(
      position.latitude, position.longitude);
  Placemark place = p[0];
  currentAddress= "${place.locality}, ${place.postalCode}, ${place.country}";
}
class business extends StatefulWidget {

  const business({
   Key? key,
    required this.email,
    required this.name,
    required this.city,
    required this.adress,
    required this.password,
    required this.type,

  })  : super(key: key);
  final String email;
  final String name;
  final String city;
  final String adress;
  final String password;
  final String type;

  @override
  _businessState createState() => _businessState();
}


class _businessState extends State<business> {

  var _image; //Holds picked image
  var imagePicker; // Holds instance of imagepicker
  var type; //Holds type of image
  Trace trace = performance.newTrace('Register Business Account');
  //ImageFromGalleryExState(this.type);

  //Method used too initialize and create the instance of the imagePicker
  @override
  void initState() {
    ImagePicker picker = ImagePicker(); //

    super.initState();
    imagePicker = new ImagePicker();
    trace.start();
  }
  final _formKey = GlobalKey<FormState>();

  //IMAGE
  UploadTask? uploadImage;

  Future<String> uploadFile() async {
    //referencia de storage
    final storageRef = FirebaseStorage.instance.ref();

    //referencia al archivo. Path en storage => nombre del item
    final imagen_upload_ref = storageRef.child(widget.email);

    //Subir archivo
    uploadImage = imagen_upload_ref.putFile(_image);

    //snapshoot que dice si la imagen ya se subio
    final snapshot = await uploadImage!.whenComplete(() {});

    //descargar url archivo
    final urlDownload = await snapshot.ref.getDownloadURL();
    return urlDownload;
  }

  @override
  Widget build(BuildContext context) {
    trace.stop();
    return MediaQuery(
        data: MediaQueryData(),
        child: MaterialApp(
            home:Scaffold(body: Directionality(
                textDirection: TextDirection.ltr,
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: ListView(
                      children: <Widget>[
                        Container(
                          child:Align(
                            alignment: Alignment.topLeft,
                            child: IconButton(
                              icon: const Icon(Icons.keyboard_return),
                              onPressed: () async {
                                await Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (contex) => signup(),
                                ),
                              );
                            },

                          ),
                          ),
                        ),
                        Container(
                            alignment: Alignment.topLeft,
                            padding: const EdgeInsets.all(5),
                            child: const Text(
                              'Register',
                              style: TextStyle(
                                fontSize: 36,
                                fontFamily: 'RobotoMono',
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 49, 68, 93),
                              ),
                            )),
                        Container(
                            alignment: Alignment.topLeft,
                            padding: const EdgeInsets.all(5),
                            child: const Text(
                              'Business',
                              style: TextStyle(
                                fontSize: 17,
                                fontFamily: 'RobotoMono',
                                fontWeight: FontWeight. normal,
                                color: Color.fromARGB(255, 49, 68, 93),
                              ),
                            )),

                         Container(
                             child: Column(
                               children: [Row(
                               children:[
                                 SizedBox(width: 10),GestureDetector(
                                 child: Container(
                                   width: 100,
                                   height: 100,
                                   decoration:
                                   BoxDecoration(color: Color.fromARGB(255, 49, 68, 93),shape: BoxShape.circle),
                                   child: _image != null
                                   //If there is an image, render it
                                       ? ClipOval(child:Image.file(
                                     //Renderiza la imagen
                                     _image,
                                     fit: BoxFit.cover,

                                   ))
                                   //If there is not an image, display camera icon

                                       : Container(
                                     decoration: BoxDecoration(
                                         color: Color.fromARGB(255, 49, 68, 93),
                                         shape: BoxShape.circle),
                                   ),
                                 ),
                               ), SizedBox(width: 50), ElevatedButton(
                                     style: TextButton.styleFrom(
                                       backgroundColor: Color.fromARGB(255, 218, 146, 82), // Background Color
                                     ),
                                     child: const Text(
                                       'Select Profile Picture',
                                       style: TextStyle(
                                           fontSize: 13,
                                           fontFamily: 'RobotoMono',
                                           fontWeight: FontWeight. bold
                                       ),
                                     ),
                                     onPressed: ()async {
                                       var source = type == ImageSourceType.camera
                                           ? ImageSource.camera
                                           : ImageSource.gallery;
                                       XFile image = await imagePicker.pickImage(
                                           source: source,
                                           imageQuality: 50,
                                           preferredCameraDevice: CameraDevice.front);

                                       //Asignamos la imagen como un estado
                                       setState(() {
                                         _image = File(image.path);
                                       });
                                     }
                                 ),],)
                          ],),),
                        Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            validator: (nit) {
                              if (nit == null ||
                                  nit.isEmpty) {
                                return 'NIT is empty';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.text,
                            onChanged: (value) {
                              nit = value;
                            },
                            style: const TextStyle(
                                fontSize: 15,
                                fontFamily: 'RobotoMono'
                            ),
                            decoration: const InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromARGB(255, 218, 146, 82),
                                    width: 1.8
                                ),
                              ),
                              labelText: 'NIT',
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 218, 146, 82),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            validator: (url) {
                              if (url == null ||
                                  url.isEmpty) {
                                return 'Webpage is empty';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.text,
                            onChanged: (value) {
                              url = value;
                            },
                            style: const TextStyle(
                                fontSize: 15,
                                fontFamily: 'RobotoMono'
                            ),
                            decoration: const InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromARGB(255, 218, 146, 82),
                                    width: 1.8
                                ),
                              ),
                              labelText: 'Web Page (URL)',
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 218, 146, 82),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            validator: (category) {
                              if (category == null ||
                                  category.isEmpty) {
                                return 'Category is empty';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.text,
                            onChanged: (value) {
                              category = value;
                            },
                            style: const TextStyle(
                                fontSize: 15,
                                fontFamily: 'RobotoMono'
                            ),
                            decoration: const InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromARGB(255, 218, 146, 82),
                                    width: 1.8
                                ),
                              ),
                              labelText: 'Category',
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 218, 146, 82),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            validator: (description) {
                              if (description == null ||
                                  description.isEmpty) {
                                return 'Description is empty';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.text,
                            onChanged: (value) {
                              description = value;
                            },
                            style: const TextStyle(
                                fontSize: 15,
                                fontFamily: 'RobotoMono'
                            ),
                            decoration: const InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromARGB(255, 218, 146, 82),
                                    width: 1.8
                                ),
                              ),
                              labelText: 'Description',
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 218, 146, 82),
                              ),
                            ),
                          ),
                        ),

                      ],
                    )),
                        Container(
                            height: 50,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: ElevatedButton(
                                style: TextButton.styleFrom(
                                  backgroundColor: Color.fromARGB(255, 49, 68, 93), // Background Color
                                ),
                                child: const Text(
                                  'REGISTER',
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontFamily: 'RobotoMono',
                                      fontWeight: FontWeight. bold
                                  ),
                                ),
                                onPressed: ()async {
                                  if (_formKey.currentState!.validate()) {
                                    try {
                                      await getCurrentLocation();
                                      UserCredential credentials = await _auth
                                          .createUserWithEmailAndPassword(
                                          email: email,
                                          password: password
                                      );
                                      img = await uploadFile();
                                      businessUser u = new businessUser(
                                          address: widget.adress,
                                          city: widget.city,
                                          name: widget.name,
                                          userName: widget.email,
                                          type: widget.type,
                                          posts: [],
                                          reviews: [],
                                          averageRaiting: 0,
                                          website: url,
                                          transactions: [],
                                          nit: nit,
                                          userUID: widget.email,
                                          imgPath: img,
                                          description: description,
                                          category: category);
                                      await FirebaseController.addUserBusiness(
                                          u, widget.email);
                                      String id = credentials.user!.uid;
                                      globals.img = u.imgPath;
                                      await Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (contex) =>
                                              MainMenu(uid: widget.email,
                                                  location: currentAddress, itemId: items),
                                        ),
                                      );
                                    }
                                    on FirebaseAuthException catch (e) {

                                    };
                                  }
                                }
                            )
                        )
            ],))
        )
    ),),);
  }
}