import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:geolocator/geolocator.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project/views/Register/signup.dart';
import 'package:intl/intl.dart';
import 'dart:ui' as ui;
import 'package:project/models/product.dart';

import 'package:project/models/globals.dart' as globals;
import '../../controllers/firebaseController.dart';
import '../../models/user.dart';
import '../item/imageWidget.dart';
import '../menu/menu.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
}

final _auth = FirebaseAuth.instance;
FirebaseAnalytics analytics = FirebaseAnalytics.instance;

String date = '';
String gender = '';
String img = '';

late String currentAddress;
late List<Product> items = [];
Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
FirebasePerformance performance = FirebasePerformance.instance;


getCurrentLocation()async {
  Position position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);

  List<Placemark> p = await geolocator.placemarkFromCoordinates(
      position.latitude, position.longitude);
  Placemark place = p[0];
  currentAddress= "${place.locality}, ${place.postalCode}, ${place.country}";
}
class person extends StatefulWidget {
  const person({
    Key? key,
    required this.email,
    required this.name,
    required this.city,
    required this.adress,
    required this.password,
    required this.type,

  })  : super(key: key);
  final String email;
  final String name;
  final String city;
  final String adress;
  final String password;
  final String type;

  @override
  State<person> createState() => _personState();
}


class _personState extends State<person> {

  var _image; //Holds picked image
  var imagePicker; // Holds instance of imagepicker
  var type; //Holds type of image
  DateTime? _selectedDate;
  TextEditingController dateinput = TextEditingController();
  Trace trace = performance.newTrace('Register Normal Account');
  //ImageFromGalleryExState(this.type);

  //Method used too initialize and create the instance of the imagePicker
  @override
  void initState() {
    ImagePicker picker = ImagePicker(); //

    super.initState();
    imagePicker = new ImagePicker();
    trace.start();
  }
  void _presentDatePicker() {
    // showDatePicker is a pre-made funtion of Flutter
    showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2020),
        lastDate: DateTime.now())
        .then((pickedDate) {
      // Check if no date is selected
      if (pickedDate == null) {
        return;
      }
      setState(() {
        // using state so that the UI will be rerendered when date is picked
        _selectedDate = pickedDate;
      });
    });
  }

  //IMAGE
  UploadTask? uploadImage;

  Future<String> uploadFile() async {
    //referencia de storage
    final storageRef = FirebaseStorage.instance.ref();

    //referencia al archivo. Path en storage => nombre del item
    final imagen_upload_ref = storageRef.child(widget.email);

    //Subir archivo
    uploadImage = imagen_upload_ref.putFile(_image);

    //snapshoot que dice si la imagen ya se subio
    final snapshot = await uploadImage!.whenComplete(() {});

    //descargar url archivo
    final urlDownload = await snapshot.ref.getDownloadURL();
    return urlDownload;
  }

  @override

  Widget build(BuildContext context) {
    trace.stop();
    return MediaQuery(
        data: new MediaQueryData(),
        child: MaterialApp(
            home:Scaffold(body: Directionality(
                textDirection: ui.TextDirection.ltr,
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: ListView(
                      children: <Widget>[
                        Container(
                          child:Align(
                            alignment: Alignment.topLeft,
                            child: IconButton(
                              icon: const Icon(Icons.keyboard_return),
                              onPressed: () async {
                                await Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (contex) => signup(),
                                  ),
                                );
                              },

                            ),
                          ),
                        ),
                        Container(
                            alignment: Alignment.topLeft,
                            padding: const EdgeInsets.all(10),
                            child: const Text(
                              'Register',
                              style: TextStyle(
                                fontSize: 36,
                                fontFamily: 'RobotoMono',
                                fontWeight: FontWeight. bold,
                                color: Color.fromARGB(255, 49, 68, 93),
                              ),
                            )),
                        Container(
                          child: Column(
                            children: [Row(
                              children:[
                                SizedBox(width: 10),GestureDetector(
                                  child: Container(
                                    width: 100,
                                    height: 100,
                                    decoration:
                                    BoxDecoration(color: Color.fromARGB(255, 49, 68, 93),shape: BoxShape.circle),
                                    child: _image != null
                                    //If there is an image, render it
                                        ? ClipOval(child:Image.file(
                                      //Renderiza la imagen
                                      _image,
                                      fit: BoxFit.cover,

                                    ))
                                    //If there is not an image, display camera icon

                                        : Container(
                                      decoration: BoxDecoration(
                                          color: Color.fromARGB(255, 49, 68, 93),
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                ), SizedBox(width: 50), ElevatedButton(
                                    style: TextButton.styleFrom(
                                      backgroundColor: Color.fromARGB(255, 218, 146, 82), // Background Color
                                    ),
                                    child: const Text(
                                      'Select Profile Picture',
                                      style: TextStyle(
                                          fontSize: 13,
                                          fontFamily: 'RobotoMono',
                                          fontWeight: FontWeight. bold
                                      ),
                                    ),
                                    onPressed: ()async {
                                      var source = type == ImageSourceType.camera
                                          ? ImageSource.camera
                                          : ImageSource.gallery;
                                      XFile image = await imagePicker.pickImage(
                                          source: source,
                                          imageQuality: 50,
                                          preferredCameraDevice: CameraDevice.front);

                                      //Asignamos la imagen como un estado
                                      setState(() {
                                        _image = File(image.path);
                                      });
                                    }
                                ),
                              ],)
                            ],),),

                        Container(
                            padding: const EdgeInsets.all(10),
                            child:TextFormField(

                              controller: dateinput,
                              //obscureText: true,
                              readOnly: true,
                              onTap: ()async {
                                DateTime? pickedDate = await showDatePicker(
                                    context: context, initialDate: DateTime.now(),
                                    firstDate: DateTime(1980), //DateTime.now() - not to allow to choose before today.
                                   lastDate: DateTime(2101)
                                );

                                if(pickedDate != null ){
                                  print(pickedDate);  //pickedDate output format => 2021-03-10 00:00:00.000
                                  String formattedDate = DateFormat('dd/MM/yyyy').format(pickedDate);
                                  print(formattedDate); //formatted date output using intl package =>  2021-03-16
                            //you can implement different kind of Date Format here according to your requirement

                                  setState(() {
                                    dateinput.text = formattedDate;
                                    date = formattedDate;//set output date to TextField value.
                                  });
                                }else{
                                  print("Date is not selected");
                                }
                                },
                              decoration: const InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color.fromARGB(255, 218, 146, 82),
                                      width: 1.8
                                  ),
                                ),
                                labelText: 'Birth Date',
                                labelStyle: TextStyle(
                                    color: Color.fromARGB(255, 218, 146, 82),
                                    fontFamily: 'RobotoMono'
                                ),

                              ),
                            )
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: Column(children: [
                            Row(children: [
                              Expanded(
                                  child: ChoiceChip(
                                      label: Text('Female',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 14)),
                                      labelPadding:
                                      EdgeInsets.symmetric(horizontal: 20),
                                      selected: gender== 'Female',
                                      onSelected: (bool selected) {
                                        setState(() {
                                          gender= (selected ? 'Female' : null)!;
                                        });
                                      },
                                      selectedColor: Color.fromARGB(255, 218, 146, 82),
                                      shape: ContinuousRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(50.0)))),
                              Expanded(
                                  child: ChoiceChip(
                                      label: Text('Male',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 14)),
                                      labelPadding:
                                      EdgeInsets.symmetric(horizontal: 20),
                                      selected: gender== 'Male',
                                      onSelected: (bool selected) {
                                        setState(() {
                                          gender= (selected ? 'Male' : null)!;
                                        });
                                      },
                                      selectedColor: Color.fromARGB(255, 218, 146, 82),
                                      shape: ContinuousRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(30.0)))),
                              Expanded(
                                  child: ChoiceChip(
                                      label: Text('Other',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 14)),
                                      labelPadding:
                                      EdgeInsets.symmetric(horizontal: 20),
                                      selected: gender== 'Other',
                                      onSelected: (bool selected) {
                                        setState(() {
                                          gender= (selected ? 'Other' : null)!;
                                        });
                                      },
                                      selectedColor: Color.fromARGB(255, 218, 146, 82),
                                      shape: ContinuousRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(30.0))))
                            ]),
                          ]),
                        ),
                        Container(
                            height: 50,
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: ElevatedButton(
                                style: TextButton.styleFrom(
                                  backgroundColor: Color.fromARGB(255, 49, 68, 93), // Background Color
                                ),
                                child: const Text(
                                  'REGISTER',
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontFamily: 'RobotoMono',
                                      fontWeight: FontWeight. bold
                                  ),
                                ),
                                onPressed: ()async {
                                  try {
                                    await getCurrentLocation();
                                    UserCredential credentials = await _auth
                                        .createUserWithEmailAndPassword(
                                        email: email,
                                        password: password
                                    );
                                    img = await uploadFile();
                                    appUser u = new appUser(
                                        address: widget.adress,
                                        birthDate: date,
                                        city: widget.city,
                                        gender: gender,
                                        name: widget.name,
                                        userName: widget.email,
                                        type: widget.type,
                                        posts: [],
                                        reviews: [],
                                        averageRaiting: 0,
                                        savedTextWaste: 0,
                                        transactions: [],
                                        treesPlanted: 0,
                                        userUID: widget.email,
                                        imgPath: img,
                                        size: '');
                                    String id = credentials.user!.uid;
                                    globals.img = img;
                                    await FirebaseController.addUserPerson(
                                        u, widget.email);

                                    await Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (contex) =>
                                            MainMenu(uid:widget.email,
                                                location: currentAddress, itemId: items,),
                                      ),
                                    );
                                  } on FirebaseAuthException catch (e) {
                                    showDialog(
                                        context: context,
                                        builder: (ctx) =>
                                            AlertDialog(
                                                title: Text(
                                                    "Ops! Register Failed"),
                                                content: Text(
                                                    "All spaces must be fill")
                                            )
                                    );
                                  };
                                  }
                            )
                        )
                      ],
                    )),

            ))
        ));
  }
}