import 'package:flutter/cupertino.dart';
import 'package:project/controllers/firebaseController.dart';
import 'package:flutter/material.dart';
import 'package:project/models/user.dart';
import 'package:project/views/profile/profileWidget.dart';
import 'package:project/views/profile/buttonWidget.dart';
import 'package:project/views/profile/appBarWidget.dart';
import 'dart:io';
import 'package:project/views/item/new_item.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:project/views/profile/login.dart';
import 'package:project/views/Register/signup.dart';
import 'package:firebase_performance/firebase_performance.dart' as performance;
class SplashViewPage extends StatefulWidget {


  @override
  _SplashViewPageState createState() => _SplashViewPageState();
}


  class _SplashViewPageState extends State<SplashViewPage> {
  //final Trace trace = performance.newTrace('SplashViewPage');

  @override
  void initState() {
    // TODO: implement initState
    //trace.start();
  }
@override
  Widget build(BuildContext context) {
     return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(
          'https://images.unsplash.com/photo-1495121605193-b116b5b9c5fe?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=388&q=80',
        ),
        fit: BoxFit.cover,),),
        child: Column(mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
          children:[Container(
  constraints: BoxConstraints.expand(
    height: 100,
    width: 200,
  ),
  padding: const EdgeInsets.all(8.0),
  color: Colors.white,
  alignment: Alignment.center,
  child: Text(
              'Swapp',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
                color: Colors.black,
              ),
            ),
), 
const SizedBox(height: 50),
 Container(
                height: 50,
                width: 200 ,
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: ElevatedButton(
                  style: TextButton.styleFrom(
                    backgroundColor: Color.fromARGB(255, 49, 68, 93), // Background Color
                  ),
                  child: const Text(
                    'LOG IN',
                    style: TextStyle(
                      fontSize: 13,
                      fontFamily: 'RobotoMono',
                        fontWeight: FontWeight. bold
                    ),
                  ),
                  onPressed: ()async {
                    Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (contex) => login(),
                                ),
                            );
                  
                  },
                )
            ),
            
            const SizedBox(height: 20),
             Container(
                height: 50,
                width: 200,
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: ElevatedButton(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.white, // Background Color
                  ),
                  child: const Text(
                    'REGISTER',
                    style: TextStyle(
                      fontSize: 13,
                      fontFamily: 'RobotoMono',
                        fontWeight: FontWeight. bold,
                       color: Color.fromARGB(255, 49, 68, 93),
                    ),
                  ),
                  onPressed: ()async {
                    Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (contex) => signup(),
                                ),
                            );
                  
                  },
                )
            ), 


])
          
    ));
  }

    Widget buildLoginButton() => ButtonWidget(
        text: 'Log in',
        onClicked: () {Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (contex) => login(),
                                ),
                            );},
      );

Widget buildRegisterButton() => ButtonWidget(
        text: 'Register',
        onClicked: () {},
      );   
      
         
}


  
  