import 'package:flutter/material.dart';
import 'package:project/views/preview/preview.dart';
import 'package:project/models/product.dart';


class CardWidget extends StatelessWidget {
  const CardWidget({Key? key, required this.item}) : super(key: key);

final Product item;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            debugPrint('Item tapped.');
          },
          child:  SizedBox(
            
            child: Stack(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              IconButton(
                                icon:Image.network(
                                  item.img, fit: BoxFit.fill, ),
                                  onPressed:() async 
                                  {
                                    Navigator.of(context).push(
                                    MaterialPageRoute(
                                    builder: (contex) => PreviewPage(item: item),
                                ),
                            );
                                  },
                                  iconSize: 200,
                              
                              ),
                              Text(
                                item.name,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    'Price: COP' + item.price.toString(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
          ),
        ),
      ),
    );
  }
}
