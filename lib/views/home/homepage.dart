import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:project/controllers/firebaseController.dart';
import 'package:project/views/profile/buttonWidget.dart';
import 'package:project/views/profile/appBarWidget.dart';
import 'package:project/views/profile/login.dart';
import 'package:project/views/profile/profileWidget.dart';
import 'package:project/views/profile/userPage.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:geolocator/geolocator.dart';
import 'package:project/models/user.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:project/views/home/cardWidget.dart';
import 'package:project/controllers/eventualConectivityController.dart';
import 'package:project/models/product.dart';
import 'package:project/views/preview/preview.dart';
import 'package:project/models/globals.dart' as globals;

import '../../models/businessUser.dart';
import '../profile/businessPage.dart';

FirebasePerformance performance = FirebasePerformance.instance;
FirebaseController controller= FirebaseController();
EventualConnectivity ec = EventualConnectivity();

class HomePage extends StatefulWidget {
  const HomePage({
    Key? key,
    required this.uid,
    required this.location,
    //required this.item,
    required this.itemList
  }) : super(key: key);

  final String uid;
  final String location;
  //final Product item;
  final List<Product> itemList;
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<HomePage> {
  final Trace trace = performance.newTrace('HomePage');

  @override
  void initState() {
    // TODO: implement initState
    trace.start();
  }

  //Get location. Taken form https://medium.com/fabcoding/get-current-user-location-in-flutter-57e202bad6db

  //final item =UserInfo.myItem
  @override
  Widget build(BuildContext context) {
    trace.stop();
    //final product =UserInfo.myUser;
    final ScrollController _controller = ScrollController();
    // final user =UserInfo.myUser;

    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;
    String picture= 'https://firebasestorage.googleapis.com/v0/b/swapp-2ef39.appspot.com/o/User_default_picture_cropped-removebg-preview.png?alt=media&token=37965234-753e-4f8a-a324-ec7d696c0f27';

    return Scaffold(
      appBar: buildAppBar(context),
      body: ListView(
        controller: _controller,

        //physics:BouncingScrollPhysics(),
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Column(children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text('Hello',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                        color: Color.fromARGB(255, 49, 68, 93),
                      ))),
              Row(
                children: [
                  Icon(Icons.place,
                      color: Color.fromARGB(255, 49, 68, 93), size: 20),
                  Text(widget.location,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          color: Color.fromARGB(255, 49, 68, 93)))
                ],
              )
            ]),
            Align(
                alignment: Alignment.centerRight,
                child: IconButton(
                  icon: Image(
                      image: NetworkImage(
                          globals.img)),
                  iconSize: 50,
                  onPressed: () async {
                    await controller.getProducts();
                    Map<String, dynamic> userData =
                        await FirebaseController.getUser(widget.uid);
                    appUser user = appUser(
                      imgPath: userData['profilePicture'],
                      name: userData['name'],
                      userUID: widget.uid,
                      gender: userData['gender'],
                      birthDate: userData['birthDate'],
                      size: userData['size'],
                      treesPlanted: userData['treesPlanted'],
                      savedTextWaste: userData['textileWasteSaved'],
                      city: userData['city'],
                      averageRaiting: userData['averageRaiting'],
                      posts: userData['posts'],
                      reviews: userData['reviews'],
                      transactions: userData['transactions'],
                      userName: userData['userName'],
                      type: userData['type'],
                      address: userData['address'],
                    );
                    
                    if(user.type.contains('Person')) {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (contex) => UserPage(user: user),
                        ),
                      );
                    }
                    else if(user.type.contains('Business')){

                      businessUser bUser = businessUser(
                        imgPath: userData["profilePicture"],
                        name: userData["name"],
                        userUID: widget.uid,
                        nit: userData ["nit"],
                        category: userData["category"],
                        description: userData["description"],
                        website: userData["website"],
                        address: userData["address"],
                        averageRaiting:userData ["averageRaiting"],
                        city: userData ["city"],
                        posts:userData["posts"],
                        reviews: userData ["reviews"],
                        transactions: userData ["transactions"],
                        userName: userData ["userName"],
                        type: userData["type"],
                      );

                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (contex) => UserBusinessPage(user: bUser),
                        ),
                      );
                    }

                  },
                )),
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Center(child: buildAllButton()),
            Center(child: buildClothingButton()),
            Center(child: buildEntretainmentButton()),
          ]),

        
          GridView.count(
            crossAxisCount: 1,
            childAspectRatio: (itemWidth / itemHeight),
            controller: new ScrollController(keepScrollOffset: false),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            children: [ ListView.separated(
            padding: const EdgeInsets.all(8),
            itemCount: items.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 300,
                child: Center(child: CardWidget(item:items[index])),
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(),
          ),],
          ),
        ],
      ),
    );
  }

  Widget buildAllButton() => ButtonWidget(
        text: 'All',
        onClicked: () {
          FirebaseCrashlytics.instance.crash();
        },
      );

  Widget buildClothingButton() => ButtonWidget(
        text: 'Clothing',
        onClicked: () {},
      );

  Widget buildEntretainmentButton() => ButtonWidget(
        text: 'Entretainment',
        onClicked: () {},
      );

    
  




  

  

  

 
}
