import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:project/controllers/firebaseController.dart';
import 'package:project/views/home/homepage.dart';
import 'package:flutter/material.dart';
import 'package:project/models/user.dart';
import 'package:project/views/profile/profileWidget.dart';
import 'package:project/views/profile/buttonWidget.dart';
import 'package:project/views/profile/appBarWidget.dart';
import 'dart:io';
import 'package:project/views/item/new_item.dart';
import 'package:project/views/cart/cart.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:project/models/product.dart';
import 'package:project/models/globals.dart' as globals;
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

//import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
FirebasePerformance performance = FirebasePerformance.instance;

class PreviewPage extends StatefulWidget {
   const PreviewPage({
    Key? key,
    required this.item,
  }) : super(key: key);

  final Product item;
  

  @override
  _PreviewPageState createState() => _PreviewPageState();
}

class _PreviewPageState extends State<PreviewPage> {
  final Trace trace = performance.newTrace('PreviewPage');
  static   FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  static FirebaseAnalyticsObserver observer = FirebaseAnalyticsObserver(analytics: analytics);
  
  ConnectivityResult result = ConnectivityResult.none;

  Future<void> _sendAnalyticsEvent() async {
    await analytics.setAnalyticsCollectionEnabled(true);
    await analytics.logSelectItem(
     itemListId: 'All items' , itemListName: 'Products', items: [AnalyticsEventItem(itemName: widget.item.name, itemCategory2: widget.item.price.toString(), itemCategory3:widget.item.size )]
    );
  }


  Future<void> sendSwapBuyEvent() async{
    if (widget.item.isSwappable)
    {
          analytics.logEvent(name: 'Swapp', parameters: null);
    }
    else{
      analytics.logEvent(name: 'Buy', parameters: null);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    trace.start();
    
    
    
  }

  @override
  Widget build(BuildContext context) {
    trace.stop();

    final ScrollController _controller = ScrollController();

    return Scaffold(
      appBar: buildAppBar(context),
      body: 
      Column(
        children: [
          Card(
            child: Container(
              height: 300,
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(widget.item.img))),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child:  Align(alignment: Alignment.topLeft,child:IconButton(
                  icon: const Icon(Icons.keyboard_return),
                  onPressed: ()  { Navigator.pop(context);
                  },

                ),
              ),
              ),
            ),
            margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0),
          ),

             const SizedBox(width: 50),
           Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                Align(alignment:Alignment.centerLeft ,child: Text( widget.item.name
                    ,
                    style: TextStyle(
                      fontSize: 24,
                      color: Color.fromARGB(255, 49, 68, 93),
                    ))),
                Center(child: Text( widget.item.size
                    ,
                    style: TextStyle(
                      fontSize: 24,
                      color: Color.fromARGB(255, 49, 68, 93),
                    ))),
              ]),
          const SizedBox(width: 50),
           Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                Align(alignment:Alignment.centerLeft ,child: sizes()),
              ]),
          const SizedBox(height: 48),
           Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [ Align(alignment: Alignment.centerLeft, child: Text('State: '+ widget.item.state
                    ,
                    style: TextStyle(
                      fontSize: 24,
                      color: Color.fromARGB(255, 49, 68, 93),
                    )),),
                
              ]),
              const SizedBox(height: 100),
          Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [buildSwappButton(), buildBuyButton()]),
        
    
        ],
      ),
   
    );
  }

  //Widget buildBackIcon() => IconButton(
    //    icon: Icon(
      //    Icons.arrow_back_rounded,
        //  color: Color.fromARGB(255, 49, 68, 93),
          //size: 20,
       // ),
        //onPressed: () async {
          //await Navigator.of(context).push(
            //MaterialPageRoute(
              //builder: (contex) => HomePage(),
            //),
          //);
        //},
     // );

  Widget buildLocationIcon() => Icon(
        Icons.place,
        color: Color.fromARGB(255, 49, 68, 93),
        size: 20,
      );

  Widget buildSwappButton() => ButtonWidget(
        text: 'Swapp',
        onClicked: () {
          sendSwapBuyEvent();
          analytics.logAddToWishlist();
        },
      );

  Widget buildBuyButton() => ButtonWidget(
        text: 'AddToCart',
        onClicked: () async {
          sendSwapBuyEvent();
          globals.cart.add(widget.item);
          analytics.logAddToCart();
           _sendAnalyticsEvent();

           result = await Connectivity().checkConnectivity();
                if (result == ConnectivityResult.none) {
                  final snackBar = SnackBar(
                    content:
                        const Text('No active connection. Payment failed :('),
                    action: SnackBarAction(
                      label: 'Ok',
                      onPressed: () {},
                    ),
                  );
           ScaffoldMessenger.of(context).showSnackBar(snackBar);
        };
        },
      );

  Widget sizes() => SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                height: 20,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.white,
                      child: Text('S',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            color: Color.fromARGB(255, 49, 68, 93),
                          )),
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.white,
                      child: Text('M',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            color: Color.fromARGB(255, 49, 68, 93),
                          )),
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.white,
                      child: Text('L',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            color: Color.fromARGB(255, 49, 68, 93),
                          )),
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.white,
                      child: Text('XL',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            color: Color.fromARGB(255, 49, 68, 93),
                          )),
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.white,
                      child: Text('XXL',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            color: Color.fromARGB(255, 49, 68, 93),
                          )),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      );

      
}