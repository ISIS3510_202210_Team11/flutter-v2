import 'package:connectivity/connectivity.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:project/views/item/imageWidget.dart';
import 'package:project/views/payment/payment.dart';
import 'dart:io';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:project/controllers/firebaseController.dart';
import 'package:project/models/user.dart';
import 'package:project/views/profile/userPage.dart';
import 'package:geolocator/geolocator.dart';

import 'package:project/models/globals.dart' as globals;
import '../../models/businessUser.dart';
import '../../models/product.dart';
import '../profile/businessPage.dart';

FirebasePerformance performance = FirebasePerformance.instance;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
}

double total = 0.0;
//state con el estado de conectividad
ConnectivityResult result = ConnectivityResult.none;
class Cart extends StatefulWidget {
  final List<Product> cart;
  final String uid;
  final String location;
  //final List<Product> items;

  Cart(this.cart, this.uid, this.location);

  @override
  cartPage createState() => cartPage(this.cart);
}

class cartPage extends State<Cart> {
  Trace trace = performance.newTrace('cart');

  cartPage(this.cart);

  //bool _enabled = false;

  List<Product> cart;

  @override
  void initState() {
    // TODO: implement initState
    trace.start();
  }

  Container pagoTotal(List<Product> cart) {
    trace.stop();
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(left: 120),
      margin: const EdgeInsets.all(10.0),
      height: 70,
      width: 170,
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 218, 146, 82),
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(50.0),
      ),
      child: Row(
        children: <Widget>[
          // Text("Total:  \$${valorTotal(_cart)}",
          //Text("Total:  ",
          Text(
            "Total  \$${valorTotal(cart)}",
            style: TextStyle(
                fontSize: 24,
                fontFamily: 'RobotoMono',
                fontWeight: FontWeight.bold,
                color: Colors.white),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    trace.stop();
    //final product =UserInfo.myUser;
    final ScrollController _controller = ScrollController();
    // final user =UserInfo.myUser;

    return Scaffold(
      body: ListView(
        controller: _controller,

        //physics:BouncingScrollPhysics(),
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Column(children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text("What's on your cart?",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                        color: Color.fromARGB(255, 49, 68, 93),
                      ))),
              Row(
                children: [
                  Icon(Icons.place,
                      color: Color.fromARGB(255, 49, 68, 93), size: 20),
                  Text(widget.location,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          color: Color.fromARGB(255, 49, 68, 93)))
                ],
              )
            ]),
            Align(
                alignment: Alignment.centerRight,
                child: IconButton(
                  icon: Image(image: NetworkImage(globals.img)),
                  iconSize: 50,
                  onPressed: () async {
                    Map<String, dynamic> userData =
                        await FirebaseController.getUser(widget.uid);
                    appUser user = appUser(
                        imgPath: userData['profilePicture'],
                        name: userData['name'],
                        userUID: widget.uid,
                        gender: userData['gender'],
                        birthDate: userData['birthDate'],
                        size: userData['size'],
                        treesPlanted: userData['treesPlanted'],
                        savedTextWaste: userData['textileWasteSaved'],
                        city: userData['city'],
                        averageRaiting: userData['averageRaiting'],
                        posts: userData['posts'],
                        reviews: userData['reviews'],
                        transactions: userData['transactions'],
                        userName: userData['userName'],
                        type: userData['type'],
                        address: userData['address']);

                    if (user.type.contains('Person')) {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (contex) => UserPage(user: user),
                        ),
                      );
                    } else if (user.type.contains('Business')) {
                      businessUser bUser = businessUser(
                        imgPath: userData["profilePicture"],
                        name: userData["name"],
                        userUID: widget.uid,
                        nit: userData["nit"],
                        category: userData["category"],
                        description: userData["description"],
                        website: userData["website"],
                        address: userData["address"],
                        averageRaiting: userData["averageRaiting"],
                        city: userData["city"],
                        posts: userData["posts"],
                        reviews: userData["reviews"],
                        transactions: userData["transactions"],
                        userName: userData["userName"],
                        type: userData["type"],
                      );

                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (contex) => UserBusinessPage(user: bUser),
                        ),
                      );
                    }
                    ;
                  },
                )),
          ]),
          Container(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: cart.length,
                    itemBuilder: (context, index) {
                      //final String imagen = _cart[index].img;
                      var item = cart[index]; //item.quantity = 0;

                      return Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 2.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width /
                                          3.5,
                                      height: 160,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          fit: BoxFit.fill,
                                          image: NetworkImage(item.img),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Column(children: <Widget>[
                                      SizedBox(
                                          width: 115,
                                          child: Text(item.name,
                                              textAlign: TextAlign.justify,
                                              style: const TextStyle(
                                                  fontSize: 17,
                                                  fontFamily: 'RobotoMono',
                                                  fontWeight: FontWeight.bold,
                                                  color: Color.fromARGB(
                                                      255, 218, 146, 82)))),
                                      SizedBox(
                                          width: 115,
                                          child: Text(
                                              '\u0024 ' + item.price.toString(),
                                              textAlign: TextAlign.justify,
                                              style: const TextStyle(
                                                  fontSize: 28,
                                                  fontFamily: 'RobotoMono',
                                                  fontWeight: FontWeight.w300,
                                                  color: Color.fromARGB(
                                                      255, 218, 146, 82)))),
                                    ]),
                                    SizedBox(width: 40),
                                    Container(
                                      decoration: const BoxDecoration(
                                        color: Color.fromARGB(255, 49, 68, 93),
                                        shape: BoxShape.circle,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          const SizedBox(
                                            height: 8.0,
                                          ),
                                          IconButton(
                                            icon: Icon(Icons.remove),
                                            iconSize: 20,
                                            onPressed: () {
                                              setState(() {
                                                _removeProduct(cart, item);
                                              });
                                              valorTotal(cart);
                                              // print(_cart);
                                            },
                                            color: Colors.white,
                                          ),
                                          SizedBox(
                                            height: 8.0,
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 30.0,
          ),
          pagoTotal(cart),
          SizedBox(
            width: 20.0,
          ),
          Container(
            height: 50,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Color.fromARGB(255, 49, 68, 93),
                    minimumSize: const Size(100, 25),
                    maximumSize: const Size(100, 25)),
                child: Text(
                  'Go pay',
                  style: TextStyle(
                      fontSize: 17,
                      fontFamily: 'RobotoMono',
                      fontWeight: FontWeight.normal,
                      color: Colors.white),
                ),
                onPressed: () async {
                  result = await Connectivity().checkConnectivity();
                  if (result == ConnectivityResult.none) {
                    final snackBar = SnackBar(
                      content: const Text(
                          'No active connection. Item not uploaded failed :('),
                      action: SnackBarAction(
                        label: 'Ok',
                        onPressed: () {},
                      ),
                    );

                    //Mostrar snackbar
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                  else{
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (contex) => Payment(
                          uid: widget.uid,
                          location: widget.location,
                          products: widget.cart,
                          total: total),
                    ),
                  );
                }}),
          ),
        ],
      ),
    );
  }
}

String valorTotal(List<Product> list) {
  total = 0.0;
  for (int i = 0; i < list.length; i++) {
    total = total + list[i].price;
  }
  return total.toStringAsFixed(2);
}

void _removeProduct(List<Product> list, Product p) {
  list.remove(p);
}

void _addProduct(List<Product> list) {
  Product p = Product(
      size: 'm',
      name: 'Example',
      isSwappable: true,
      state: '',
      img:
          'https://media.revistagq.com/photos/6099151446471da0248861b8/master/w_3131,h_4236,c_limit/mr-mood-camisa-lino.jpg',
      price: 20.000,
      tags: []);
  list.add(p);
}
